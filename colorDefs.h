#ifndef COLORDEFS_H
#define COLORDEFS_H

#include <QColor>

struct ColorDefs
{
    QColor white;
    QColor black;
    QColor gray;
    QColor lightGray;
    QColor red;
    QColor lightRed;
    QColor yellow;
    QColor orange;
    QColor lightBlue;
    QColor blue;


    void setColors()
    {
        white.setRgb( 255, 255, 255 );
        black.setRgb( 0, 0, 0 );
        gray.setRgb( 150, 150, 150 );
        lightGray.setRgb( 200, 200, 200 );
        red.setRgb( 175, 0, 0 );
        lightRed.setRgb( 255, 200, 200 );
        yellow.setRgb( 255, 255, 155 );
        orange.setRgb( 255, 100, 0 );
        lightBlue.setRgb( 200, 200, 255 );
        blue.setRgb( 0, 0, 175 );
    }
};

#endif // COLORDEFS_H
