#include <QtGui/QApplication>
#include <QImage>
#include <QPainter>
#include <QRect>
#include <QLine>
#include <QColor>
#include <QBrush>
#include <QPoint>
#include <QFont>
#include <QString>
#include <QStringList>
#include <QFile>
#include <QTextStream>
//#include <QFontMetrics>

#include "qmlapplicationviewer.h"

#include "colorDefs.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QScopedPointer<QApplication> app(createApplication(argc, argv));

    QmlApplicationViewer viewer;
    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);
    viewer.setMainQmlFile(QLatin1String("qml/frameTimeSeries/main.qml"));
    viewer.showExpanded();

// ******************    The following is my code:    ******************

    /*****************************************************
     * Enter the number of frames and number of non-     *
     * groundtruth result lists. if the number of result *
     * lists is not provided, it will be taken as 1.     *
     * Text files should be named as folows:             *
     *     groundtruth.txt : Text file with ground truth *
     *     match<n>.txt    : Text file with match frames *
     *     result<n>.txt   : Text file with results      *
     *     names.txt       : Text File with names of     *
     *                       result algorithms           *
     *                                                   *
     * The text files should have one number per row,    *
     * i.e., the frame number. The match files will have *
     * two per row, i.e., the two matched frame numbers. *
     *****************************************************/

    int numRes = 1;

    if ( argc < 2 )
    {
        return app->exec();
    }
    else if ( argc == 3 )
    {
        numRes = atoi( argv[2] );
    }

    int numFrames = atoi( argv[1] );

    // Reading in the values

    QStringList stringList;
    QString Folder = "/home/mahesh/";
    QString presFile;

    // Read the ground truth
    presFile = Folder + "groundTruth.txt";
    QFile groundTruth( presFile );
    groundTruth.open(QIODevice::ReadOnly);
    QTextStream textStream(&groundTruth);
    while ( true )
    {
        QString line = textStream.readLine();
        if (line.isNull())
            break;
        else
            stringList.append(line);
    }

    QList<int> changePt;
    changePt.reserve( stringList.length() );

    int i = 0;
    foreach ( QString str, stringList )
    {
        changePt[i] = str.toInt();
        i++;
    }

    groundTruth.close();

    // Read in results
    QList< QList<int> > changePtRes;
    changePtRes.reserve( numRes );
    QString num;
    int j = 0;
    for( i = 0; i < numRes; ++i)
    {
        num.setNum( (i+1) );
        presFile = Folder + "result" + num + ".txt";
        QFile resultI( presFile );
        resultI.open(QIODevice::ReadOnly);
        QTextStream textStream(&resultI);
        while ( true )
        {
            QString line = textStream.readLine();
            if (line.isNull())
                break;
            else
                stringList.append(line);
        }

        changePtRes[i].reserve( stringList.length() );

        j = 0;
        foreach ( QString str, stringList )
        {
            changePtRes[i][j] = str.toInt();
            j++;
        }

        resultI.close();
    }

    // End of reading in the values
    // Basic defs
    ColorDefs colors;
    colors.setColors();

    QImage img( (numFrames+700), ((numRes+3)*230), QImage::Format_RGB32 );
    int basePixelX = 550;
    int basePixelY = ( numRes+2 )*230;

    //img.fill( white );
    img.fill( colors.white.rgb() );
    QPainter paint( &img );
    QFont roman55( "Helvetica"/*"Times"*/, 55 );

    // Drawing the base line
    QLine baseLine1( (basePixelX-2), basePixelY,
                     (basePixelX+numFrames+20), basePixelY );
    QLine baseLine2( (basePixelX-2), (basePixelY-1),
                     (basePixelX+numFrames+20), (basePixelY-1) );
    QLine baseLine3( (basePixelX-2), (basePixelY-2),
                     (basePixelX+numFrames+20), (basePixelY-2) );
    QLine baseLine4( (basePixelX-2), (basePixelY-3),
                     (basePixelX+numFrames+20), (basePixelY-3) );

    paint.setPen( colors.black );
    paint.drawLine( baseLine1 );
    paint.drawLine( baseLine2 );
    paint.drawLine( baseLine3 );
    paint.drawLine( baseLine4 );

    // Draw the frame numebers etc. and write the text

    paint.setFont( roman55 );
    QStringList axisNrs;
    axisNrs.clear();
    axisNrs.reserve( static_cast<int> (numFrames/1000) );
    QPoint axisPt( (basePixelX-30), (basePixelY+100) );

    for( i = 0; i < axisNrs.size(); ++i)
    {
        if(i == 0)
            axisNrs[i].setNum(1);
        else
            axisNrs[i].setNum( i*1000 );

        paint.drawText( axisPt, axisNrs[i] );
        axisPt.setX( (basePixelX+920+(i*1000)) );
    }

    paint.setPen( colors.gray );
    paint.setPen( Qt::DotLine );

    QLine axisLine1, axisLine2, axisLine3;

    int xpt = basePixelX;
    int ypt = basePixelY + 5;
    for( i = 0; i < axisNrs.size(); ++i)
    {
        axisLine1.setLine( xpt, ypt, xpt, 50 );
        axisLine2.setLine( (xpt+1), ypt, (xpt+1), 50 );
        axisLine3.setLine( (xpt-1), ypt, (xpt-1), 50 );
        paint.drawLine( axisLine1 );
        paint.drawLine( axisLine2 );
        paint.drawLine( axisLine3 );
        xpt += 1000;
    }

    paint.setPen( Qt::SolidLine );

    // Matching

    // Groundtruth

    // Test Results
    paint.setPen( colors.red );

    QRect event;

    for( i = 0; i < numRes; ++i )
    {
        for( j = 0; j < changePtRes[i].size(); ++j  )
        {
            event.setCoords( ( (basePixelX-3) + (changePtRes[i][j]/10) ),
                             ( (i*230) + 30 ),
                             ( (basePixelX+3) + (changePtRes[i][j]/10) ),
                             ( (i*230) + 230 )                           );
            paint.drawRoundedRect( event, 6, 6 );
            paint.fillRect( event, colors.red );
        }
    }

    QImage saveImg = img.scaledToWidth( 2000 );
    QString path = "/home/mahesh/res_pic.png";
    saveImg.save( path );

    return app->exec();
}
