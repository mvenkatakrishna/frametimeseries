// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    width: 360
    height: 360

   Text {
        id: text1
        x: 105
        y: 156
        color: "#000000"
        text: qsTr("Processed")
        font.pixelSize: 16
        Timer {
            interval: 1000; running: true; repeat: false
        }
     }
   /*Text {
        text: qsTr("Hello World")
        anchors.centerIn: parent
    }*/
    MouseArea {
        anchors.fill: parent
        onClicked: {
            Qt.quit();
        }
    }
}
